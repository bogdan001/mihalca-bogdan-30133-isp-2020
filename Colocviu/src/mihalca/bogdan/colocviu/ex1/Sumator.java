package mihalca.bogdan.colocviu.ex1;

import javax.swing.*;


public class Sumator extends JFrame {


    static JTextArea txt1;
    static JTextArea txt2;
    static JTextArea txt3;
    JButton doSmt;
    static int width = 80;
    static int height = 20;
    static int nr1 = 14;
    static int nr2 = 10;
    static int sum=nr1+nr2;

    Sumator() {
        setTitle("Sumator");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(380, 400);
        setVisible(true);
    }

    private void init() {
        this.setLayout(null);


        txt1 = new JTextArea();
        txt1.setBounds(80, 20, width, height);
        txt1.setEnabled(true);
        txt1.setText(String.valueOf(nr1));

        txt2 = new JTextArea();
        txt2.setBounds(80, 50, width, height);
        txt2.setEnabled(true);
        txt2.setText(String.valueOf(nr2));

        txt3 = new JTextArea();
        txt3.setBounds(80, 80, width, height);
        txt3.setEnabled(true);


        doSmt = new JButton("Press");
        doSmt.setBounds(10, 100, 145, 50);
        doSmt.addActionListener(new doSmtOnPress());

        add(txt1);
        add(txt2);
        add(txt3);
        add(doSmt);

    }
        public static void main (String[] args){
            new Sumator();

        }

    }
